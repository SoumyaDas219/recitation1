/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Soumya
 */
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.geometry.Pos;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.stage.Stage;

/**
 *
 * @author Soumya
 */
public class Main extends Application {
    
    @Override
    public void start(Stage primaryStage) {
        GridPane pane=new GridPane();
        pane.getStyleClass().add("Button");
        pane.setAlignment(Pos.CENTER);
        pane.setPadding(new Insets(12,13,14,15));
        pane.setHgap(6);
        pane.setVgap(6);
        
        pane.add(new Label("First name"),0,0);
        pane.add(new TextField(),1,0);
        pane.add(new Label("Last name"),0,1);
        pane.add(new TextField(),1,1);
        pane.add(new Label("Age"),0,2);
        pane.add(new TextField(),1,2);
        Button okbt=new Button("Login");
        pane.add(okbt,1,3);
        pane.setStyle("-fx-border-color:red");
        
        Scene scene= new Scene(pane);
       
        primaryStage.setTitle("Login");
        primaryStage.setScene(scene);
        primaryStage.setResizable(true);
        primaryStage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}
